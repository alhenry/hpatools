# HPA-Database

This project contains scripts to generate and query the Human Protein Atlas as an SQLite database. It supplements the HPA with single cell Type Tissue Cluster data from HPA and gene tissue specific expression from GTEx.

The XML file containing the HPA data can be downloaded here: https://www.proteinatlas.org/download/proteinatlas.xml.gz
The singleCellType Tissue Cluster data can be downloaded here: https://www.proteinatlas.org/download/rna_single_cell_type_tissue.tsv.zip
The GTEx file containing the Gene-Tissue specific expressions can be downloaded here: https://storage.googleapis.com/gtex_analysis_v8/rna_seq_data/GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_median_tpm.gct.gz


The scripts have been tested in python 3.7. To install the script which creates processes the HPA xml file and generates an SQLlite database as a commandline program, Python 3.7 or higher is required. 
Dependencies can be installed via pip with the following command.

```
python -m pip install --upgrade -r requirements.txt
```

Followed by the installation of the script

```
python -m pip install -e .
```

Alternatively, a new conda environment can be created from the enclosed .yml file.

```
conda env create -f environment.yml
```

Activate the newly created environment and install the script similar to the example above.

```
conda activate hpatools
```

Followed by the installation of the script.

```
python -m pip install -e .
```

This runs the editible install which enables the possibility to update the script without newly installying the script another time. The pogram can be started from the commandline.
To process the XML file and generate an SQLite database, the XML file needs to be decompressed first. The scRNA file also needs to be decompressed while the GTEx file can either be decompressed or gzipped (.gz). For testing purposes, the repository contains the first ten entries of the proteinatlas.xml file.
The script can be tested using the following command:

```
hpa_create --HPA_database test_db.db --infile first_ten_entries.xml --scRNA_file rna_single_cell_type_tissue.tsv --GTEx_file GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_median_tpm.gct
```

v.03 using a single 2 GHz Intel Core i5, it takes around ~16h to generate the full SQLite database.
v.05 speed test for the current version has not been done yet.

HPA_query.py queries the HPA database for RNA expression values and RNA tissue specificity values. To do that the user needs to provide a file containing Ensembl Gene Identifiers in one column without a header (see test_query.txt). It will generate two output files with the prefix 'HPA-exp_' and 'HPA-spec_' containing the gene-tissue expression and gene-tissue specificity data respectively. 
In future the HPA_query.py script will be further imrpoved by including exclusion criteria and Uniprot accession identifiers as access point.

```
python HPA_query.py --HPA_database HPA_v05.db --infile test_query.txt --outpath ./
```

v.05
Read in the GTEx file GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_median_tpm.gct.gz and link it to the Gene_information table and Tissue_information table.

v.04
Create a helper/lookup dict to check if the combination of assay data is unique per RNA Expression.
Extract and store assembly and genecodeVersion information for each gene/entry.
Process cellTypeDistribution and add to rna_specificity_dict
Process cellTypeSpecificity and add to rna_specificty_dict
Extract scRNA celltype specific cluster information and expression, store both in a new table
Fix issue that the same tissue names appear multiple times which fox example leads to an issue in which scRNA T-Cells are the T-Cells lineage assigned to even though these should be two distinct tissues while on the other hand Adipose & soft tissue occurs multiple times. The uniq identifier sequence that is stored in the tissue_helper_dict is now all capitalized to avoid issues if in the HPA xml smaller case letters are used at individual positions. The uniq identifier sequence that is stored in the tissue_helper_dict now includes the assay  index number but the assays consensusTissue(0) and Tissue(1), humanBrain(2) and mouseBrain(5) and pigBrain(7), humanBrainRegional(3) and mouseBrainRegional(4) and pigBrainRegional(6) are treated equally respectively.

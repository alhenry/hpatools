#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 21 15:58:21 2020
Last Updated on 2021, Tuesday Jan. 12th

@author: christiangross

This script is reading in the HPA proteinatlas.xml file and creates an sqlite database
Currently RNA tissue specificity, Tissue, Protein, RNA Expression, RNA Samples,scRNA Expression
are retrieved.
v.04
DONE:   Create a helper/lookup dict to check if the combination of assay data is unique per RNA
        Expression.
DONE:   Extract and store assembly and genecodeVersion information for each gene/entry.
DONE:   Process cellTypeDistribution and add to rna_specificity_dict
DONE:   Process cellTypeSpecificity and add to rna_specificty_dict
DONE:   Extract scRNA celltype specific cluster information and expression, store both in a new
        table
DONE:   Fix issue that the same tissue names appear multiple times which fox example leads to
        an issue in which scRNA T-Cells are the T-Cells lineage assigned to even though
        these should be two distinct tissues while on the other hand Adipose & soft tissue
        occurs multiple times.
            The uniq identifier sequence that is stored in the tissue_helper_dict is now all 
            capitalized to avoid issues if in the HPA xml smaller case letters are used at 
            individual positions.
            The uniq identifier sequence that is stored in the tissue_helper_dict now includes 
            the assay  index number but the assays consensusTissue(0) and Tissue(1), humanBrain(2) 
            and mouseBrain(5) and pigBrain(7), humanBrainRegional(3) and mouseBrainRegional(4) and
            pigBrainRegional(6) are treated equally respectively.
v.05
DONE:   Read in the GTEx file GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_median_tpm.gct.gz 
        and link it to the Gene_information table and Tissue_information table.
            Generator function which will help me to correctly read in the GTEx file as pandas 
            dataframe. I will arrange all the columns with the expression values underneath of 
            each other thus that there are only 4 columns left. Name, Description, Tissue and 
            Expression. These will be converted that the Name column (these are ensembl ids) 
            contains links to the Gene_information.Ensembl_ID, and the Tissue column to the 
            Tissue_information.tissue_name column then use the pd.DataFrame.to_sql() to add 
            this to a pre-initialized sql table. This means that the link between the GTEx 
            expressions and the rest of the HPA will be mainly over the Gene_information.Ensembl_ID.
            https://storage.googleapis.com/gtex_analysis_v8/rna_seq_data/GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_median_tpm.gct.gz
TODO:   For all external files, implement a handle which can deal with .gz files and normal text files.
TODO:   retrieval and storage of antibody staining, antibody information and pathology information.
TODO:   instead of handing over a dictionary to the retrieval functions and specificing in the
        docstring which dictionary I should be handed over, I should replace the dictionary with
        required dict because they are globally defined and the functions do not need to be used
        for any other situation.
TODO:   Break the main() function into smaller bits and pieces.
TODO:   Create a function to fill in rna_specificity_dict and linker table which can be used for
        scRNA and RNA expression, currently it is done manually twice.
TODO:   Create a function which extracts the assay information for RNAexpression and scRNA.
TODO:   Implement fuzzy check of GTEx tissues and already existing tissues in the tissue_info_table
"""
###############################################################
#Import Modules
###############################################################
import os
import sys
import gzip
import time
import argparse
import xml.etree.ElementTree as ET
import numpy as np
import pandas as pd
#import pandas._libs.tslibs.base#is added to prevent compilation errors
#import dateutil.easter#is added to prevent compilation errors
#import cmath#is added to prevent compilation errors
from sqlalchemy import create_engine
from sqlalchemy import MetaData
from sqlalchemy import Table
from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import ForeignKey
from sqlalchemy import Float

###############################################################
#Table dictionary Initializtations
###############################################################
#Tables are intialized as dictionaries containing lists which eventually will be
#converted to pandas dataframes and SQLlite data tables

#This is the highest level table, containing no foreign keys. It contains information of the gene
#(entry).
gene_info_dict = {'Ensembl_ID':[],
                  'HPA_version':[],
                  'URL':[],
                  'Gene_ID':[],
                  'Name':[],
                  'assembly':[],
                  'gencodeVersion':[]
                  }

#This table is a high level table containing no foreign keys. Connects to rnaExpression but can also
#also connect to tissueExpression, pathologyExpression when implemented.
assay_info_dict = {'Assay_ID':[],
                   'source':[],
                   'technology':[],
                   'assayType':[]
                   }

#This table contains the Specificity and Distribution of RNA expression, if available the tissue ID is provided.
rna_specificity_dict = {'RNA_Specificity_ID':[],
                        'rnaSpecificity_description':[],
                        'rnaSpecificity_specificity':[],
                        'rnaDistribution_description':[],
                        'Tissue_ID':[]
                        }

#This is a high level table contains the information about different tissues, celllines, regions, cell lineages.
#When implemented it can connect to (antibody, tissueExpression,cellExpression,pathologyExpression,tissueExpression)
tissue_info_dict = {'Tissue_ID':[],
                    'tissue_name':[],
                    'ontologyTerms':[],
                    'cellosaurusID':[],
                    'additional_information':[]
                   }

#This table contains the different RNA Expression values and their units, because there are RNA expression
#values for all combinations of Tissues, Genes and Assays, their IDs are also represented here for an easier
#access.
value_dict = {'Value_ID':[],
              'Tissue_ID':[],
              'Gene_ID':[],
              'Assay_ID':[],
              'normalizedRNAExpression':[],
              'proteinCodingRNAExpression':[],
              'RNAExpression':[],
              'norm-RNA-exp_unit':[],
              'prot-CodingRNA-exp_unit':[],
              'RNA-exp_unit':[],
              'Text':[]
             }

#This table contains the different RNA Samples because there are RNA Samples for all combinations of Values,
#Tissues, Genes and Assays, their IDs are also represented here for an easier access.
RNA_sample_dict = {'RNASample_ID':[],
                   'Tissue_ID':[],
                    'sampleId':[],
                    'Gene_ID':[],
                    'Assay_ID':[],
                    'Value_ID':[],
                    'expRNA':[],
                    'sex':[],
                    'age':[],
                    'RNA-exp_unit':[]
                   }

#This table is a linker table between Gene-/Assay information and RNA specificity
gene_assay_rnaSpecificity_link_dict = {'Gene-RNASpecificity-link_ID':[],
                                       'Gene_ID':[],
                                       'Assay_ID':[],
                                       'RNA_Specificity_ID':[]
                                       }

#This table contains a foreign key but can also be used as an access into the database.
#This table ontains the protein ID as defined in the speciofied database.
Protein_information_dict = {'Protein_info_ID':[],
                            'Gene_ID':[],
                            'database':[],
                            'protId':[]
                            }

#This is a linker table between the protein information and protein class because
#protein class has many more values than protein information.
protein_linker_dict = {'Protein_linker_ID':[],
                       'Protein_info_ID':[],
                       'Protein_classes_ID' : []
                       }

#This table contains information about the protein class and protein evidence as
#provided by different databases.
Protein_class_dict = {'Protein_classes_ID':[],
                      #'Protein_linker_ID':[],
                      'source':[],
                      'ProteinClass_description':[]
                      }

#This table contains the scRNA expression per cellType cluster
scRNA_cellType_dict = {'scRNA_exp_ID':[],
                    'Tissue_ID':[],
                     'Gene_ID':[],
                     'Read_count':[],
                     'pTPM':[]
                    }

#These dictionaries are used to store unique row identifiers and their indexes in the table as
#values.This speeds up the lookup to check if a row has already occurred before.
protein_helper_dict = {}
tissue_helper_dict = {}
rna_specificity_helper_dict = {}
assay_helper_dict = {}
source_helper_dict = {}
technology_helper_dict = {}

###############################################################
#Functions
###############################################################
def main():
    """
    The main entry point for the program
    """
    ###############################################################
    #ArgParse
    ###############################################################
    parser = set_args()
    args = parse_args(parser)

    ###############################################################
    #Iteration over the XML file and retrieval of data
    ###############################################################

    #check if database already exists. if yes, stop
    if os.path.exists(args.HPA_database):
        sys.exit('The HPA database already exists. \n %s'%(args.HPA_database))
        
    #check if GTex file exists, if no, stop
    if not os.path.exists(args.GTEx_file):
        sys.exit('The current version v.05 requires the GTex Gene Tissue expression file to be\
                 present but it cannot be found int the provded path.')

    #reads in the single Cell RNA cluster file as a pd.DataFrame
    scRNA_df = pd.read_csv(args.scRNA_file,sep='\t',index_col='Gene')
    
    #opens the XML file as an Element Tree iterator
    tree = ET.iterparse(os.path.join(args.infile),events=("start","end"))

    #Takes start time to monitor the running script.
    start = time.time()

    #there are a total of 19670 entries, each standing for a unique Ensembl_ID
    counter = 0
    for event,entry in tree:
        #In the background the xml tree is constructed which makes it possible to parse from start to end of one
        #entry and then dissect the tree per entry
        if (event == 'end') and (entry.tag == 'entry'):
            #At this point a whole entry is loaded and its xml tree is constructed.
            #Counter which returns the passed time every 1000 gene entries.
            if counter%1000 == 0:
                print(counter,"genes processed, Hours passed:",(time.time()-start)/3600)
                counter += 1
            else:
                counter += 1

            # """
            # #!!!ignore copyright and any other information on this level for now
    
            # if os.path.split(entry.attrib['url'])[-1]=='ENSG000000x20256':
            #     break
            # else:
            #     entry.clear()
            # """

            #splits the url attrib and retrieves the ensembl gene id which is stored there
            gene_info_dict['Ensembl_ID'].append(os.path.split(entry.attrib['url'])[-1])
            #retrieves the first entry behind name and accesses its text which should be the gene name
            gene_info_dict['Name'].append(retrieve_single_entry('name',entry).text)
            #uses the current length of the Gene_ID as index, in that way the index is 0-based.
            current_gene_index = len(gene_info_dict['Gene_ID'])
            gene_info_dict['Gene_ID'].append(current_gene_index)
            gene_info_dict['HPA_version'].append(entry.attrib['version'])
            gene_info_dict['URL'].append(entry.attrib['url'])
            gene_info_dict['assembly'].append(retrieve_single_entry('identifier',entry).attrib['assembly'])
            gene_info_dict['gencodeVersion'].append(
                retrieve_single_entry('identifier',entry).attrib['gencodeVersion'])

            #retrieves the protein information if available.
            num_prot_ids = retrieve_proteininfo(entry,Protein_information_dict,current_gene_index)
            #only when a protein with an ID is defined, it tries to retrieve protein class data.
            if num_prot_ids > 0:
                #retrieve list of protein IDs in the protein info table and retrieve protein class data
                prot_ids = Protein_information_dict['Protein_info_ID'][-num_prot_ids:]
                for current_prot_id in prot_ids:
                    retrieve_proteinclass(entry,Protein_class_dict,current_prot_id)

            #iterfind searches within the xml tree for the tag rnaExpression and iterates over all its children.
            #rnaExpression contains the RNA expression values for different tissues
            for element in entry.iterfind('rnaExpression'):
                ####################################################################
                #!!! Could be outsourced to a function
                #Filling assay_info_dict
                ####################################################################   
                #per rnaExpression element, the assayType is unique
                #create unique identifier string for the assay entries
                uniq_assay_str = join_attributes_to_str(element).upper()
                #Check if unique identifier already exists, otherwise use already existing assay index
                if uniq_assay_str not in assay_helper_dict:
                    current_assay_index = len(assay_info_dict['Assay_ID'])
                    assay_helper_dict[uniq_assay_str] = current_assay_index
                    assay_info_dict['Assay_ID'].append(current_assay_index)
                    assay_info_dict['source'].append(element.attrib['source'])
                    assay_info_dict['technology'].append(element.attrib['technology'])
                    assay_info_dict['assayType'].append(element.attrib['assayType'])
                else:
                    #Retrieves index of the current assay.
                    current_assay_index = assay_helper_dict[uniq_assay_str]

                #RNA Specificity and Distribution are stored in the same row in the same table. RNA
                #distribution occurs only once and must be retrieved first to repeat its value as
                #many times as there is RNA
                #Specificity.
                subelem = retrieve_single_entry('rnaDistribution',element)
                if subelem is not None:
                    rna_distribution_str = subelem.text.strip()+';'+subelem.attrib['description']

                ####################################################################
                #!!! Could be outsourced to a function
                #Filling rna specificity table and gene_assay_rna specificity linker
                ####################################################################
                #Per rnaExpression, there is at most one rnaSpecificity and rnaDistribution element,
                subelem = retrieve_single_entry('rnaSpecificity',element)
                if subelem is not None:
                    #check if there are children nodes, if yes retrieve the tissue information otherwise empty string.
                    if len(list(subelem)) > 0:
                        #this function returns a list of Tissue_IDs therefore I need to fill in the data per ID
                        for tissue_id in retrieve_tissue(subelem,tissue_info_dict,current_assay_index):
                            #create an identifier of the current element.
                            uniq_specificity_str = (join_attributes_to_str(subelem)+rna_distribution_str+str(tissue_id)).upper()
                            #check identifier in lookup table.
                            if uniq_specificity_str not in rna_specificity_helper_dict:
                                #if new identifier, store new one and all the data
                                current_specificity_id = len(rna_specificity_dict['RNA_Specificity_ID'])
                                rna_specificity_helper_dict[uniq_specificity_str] = current_specificity_id
                                rna_specificity_dict['RNA_Specificity_ID'].append(current_specificity_id)
                                rna_specificity_dict['rnaSpecificity_description'].append(subelem.attrib['description'])
                                rna_specificity_dict['rnaSpecificity_specificity'].append(subelem.attrib['specificity'])
                                rna_specificity_dict['Tissue_ID'].append(tissue_id)
                                rna_specificity_dict['rnaDistribution_description'].append(rna_distribution_str)
                                gene_assay_rnaSpecificity_link_dict['RNA_Specificity_ID'].append(current_specificity_id)
                            else:
                                #if old identifier, retrieve index.
                                gene_assay_rnaSpecificity_link_dict['RNA_Specificity_ID'].append(
                                    rna_specificity_helper_dict[uniq_specificity_str])

                            #Fill in the linker table per Gene/ Assay combination for which RNA Specificity is defined.
                            gene_assay_rnaSpecificity_link_dict['Gene-RNASpecificity-link_ID'].append(
                                len(gene_assay_rnaSpecificity_link_dict['Gene-RNASpecificity-link_ID']))
                            gene_assay_rnaSpecificity_link_dict['Gene_ID'].append(current_gene_index)
                            gene_assay_rnaSpecificity_link_dict['Assay_ID'].append(current_assay_index)
                    else:
                        #If now tissue is available retrieve only the RNA Specificity and Distribution.
                        #create an identifier of the current element.
                        uniq_specificity_str = (join_attributes_to_str(subelem)+rna_distribution_str).upper()
                        if uniq_specificity_str not in rna_specificity_helper_dict:
                            #if new identifier, store new one and all the data
                            current_specificity_id = len(rna_specificity_dict['RNA_Specificity_ID'])
                            rna_specificity_helper_dict[uniq_specificity_str] = current_specificity_id
                            rna_specificity_dict['RNA_Specificity_ID'].append(current_specificity_id)
                            rna_specificity_dict['rnaSpecificity_description'].append(subelem.attrib['description'])
                            rna_specificity_dict['rnaSpecificity_specificity'].append(subelem.attrib['specificity'])
                            rna_specificity_dict['Tissue_ID'].append('')
                            rna_specificity_dict['rnaDistribution_description'].append(rna_distribution_str)
                            gene_assay_rnaSpecificity_link_dict['RNA_Specificity_ID'].append(current_specificity_id)
                        else:
                            #if old identifier, retrieve index.
                            gene_assay_rnaSpecificity_link_dict['RNA_Specificity_ID'].append(
                                rna_specificity_helper_dict[uniq_specificity_str])

                        #Fill in the linker table per Gene/ Assay combination for which RNA Specificity is defined.
                        gene_assay_rnaSpecificity_link_dict['Gene-RNASpecificity-link_ID'].append(
                            len(gene_assay_rnaSpecificity_link_dict['Gene-RNASpecificity-link_ID']))
                        gene_assay_rnaSpecificity_link_dict['Gene_ID'].append(current_gene_index)
                        gene_assay_rnaSpecificity_link_dict['Assay_ID'].append(current_assay_index)

                #if there is data, then it occurs multiple times therefore I have to iterate over
                #all of them
                for subelem in element.iterfind('data'):
                    #data does not have any information on its own but contains several children
                    #which differ depending on the assay: tissue, level, RNASample,                    
                    current_tissue_index = retrieve_tissue(subelem,tissue_info_dict,current_assay_index)[0]
                    value_dict['Tissue_ID'].append(current_tissue_index)
                    value_dict['Gene_ID'].append(current_gene_index)
                    value_dict['Assay_ID'].append(current_assay_index)
                    #fill in RNA Expression values to value_dict.
                    current_value_id = retrieve_level(subelem,value_dict)
                    #fill in tissue and assay id to RNA_sample_dict
                    retrieve_RNAsample(subelem,
                                       RNA_sample_dict,
                                       current_tissue_index,
                                       current_gene_index,
                                       current_assay_index,
                                       current_value_id)
            #Here I am iterating over summary data of the single CellType Expression values
            for element in entry.iterfind('cellTypeExpression'):
                ####################################################################
                #!!! Could be outsourced to a function
                #Filling assay_info_dict
                ####################################################################                
                #per rnaExpression element, the assayType is unique
                #create unique identifier string for the assay entries
                """!!!
                subelem = retrieve_single_entry('rnaDistribution',element)
                if subelem is not None:
                    rna_distribution_str = subelem.text.strip()+';'+subelem.attrib['description']
                join_attributes_to_str(subelem)+rna_distribution_str+str(tissue_id)
                """
                
                uniq_assay_str = join_attributes_to_str(element).upper()
                #Check if unique identifier already exists, otherwise use already existing assay index
                if uniq_assay_str not in assay_helper_dict:
                    current_assay_index = len(assay_info_dict['Assay_ID'])
                    assay_helper_dict[uniq_assay_str] = current_assay_index
                    assay_info_dict['Assay_ID'].append(current_assay_index)
                    assay_info_dict['source'].append('')
                    assay_info_dict['technology'].append(element.attrib['technology'])
                    assay_info_dict['assayType'].append(element.attrib['assayType'])
                else:
                    #Retrieves index of the current assay.
                    current_assay_index = assay_helper_dict[uniq_assay_str]
                    
                #cellTypeSpecificity and cellTypeDistribution are stored in the same row in the same table.
                #distribution occurs only once and must be retrieved first to repeat its value as
                #many times as there is scRNA
                #Specificity.
                subelem = retrieve_single_entry('cellTypeDistribution',element)
                if subelem is not None:
                    rna_distribution_str = subelem.text.strip()
                ####################################################################
                #!!! Could be outsourced to a function
                #Filling rna specificity table and gene_assay_rna specificity linker
                ####################################################################
                subelem = retrieve_single_entry('cellTypeSpecificity',element)
                if subelem is not None:
                    #check if there are children nodes, if yes retrieve the tissue information otherwise empty string.
                    if len(list(subelem)) > 0:
                        #this function returns a list of Tissue_IDs therefore I need to fill in the data per ID
                        for tissue_id in retrieve_tissue(subelem,tissue_info_dict,current_assay_index):
                            #create an identifier of the current element.
                            uniq_specificity_str = (join_attributes_to_str(subelem)+rna_distribution_str+str(tissue_id)).upper()
                            #check identifier in lookup table.
                            if uniq_specificity_str not in rna_specificity_helper_dict:
                                #if new identifier, store new one and all the data
                                current_specificity_id = len(rna_specificity_dict['RNA_Specificity_ID'])
                                rna_specificity_helper_dict[uniq_specificity_str] = current_specificity_id
                                rna_specificity_dict['RNA_Specificity_ID'].append(current_specificity_id)
                                #rna_specificity_dict['rnaSpecificity_description'].append(subelem.attrib['description'])
                                #rna_specificity_dict['rnaSpecificity_specificity'].append(subelem.attrib['specificity'])
                                rna_specificity_dict['rnaSpecificity_description'].append('')
                                rna_specificity_dict['rnaSpecificity_specificity'].append(subelem.attrib['category'])
                                rna_specificity_dict['Tissue_ID'].append(tissue_id)
                                rna_specificity_dict['rnaDistribution_description'].append(rna_distribution_str)
                                gene_assay_rnaSpecificity_link_dict['RNA_Specificity_ID'].append(current_specificity_id)
                            else:
                                #if old identifier, retrieve index.
                                gene_assay_rnaSpecificity_link_dict['RNA_Specificity_ID'].append(
                                    rna_specificity_helper_dict[uniq_specificity_str])

                            #Fill in the linker table per Gene/ Assay combination for which RNA Specificity is defined.
                            gene_assay_rnaSpecificity_link_dict['Gene-RNASpecificity-link_ID'].append(
                                len(gene_assay_rnaSpecificity_link_dict['Gene-RNASpecificity-link_ID']))
                            gene_assay_rnaSpecificity_link_dict['Gene_ID'].append(current_gene_index)
                            gene_assay_rnaSpecificity_link_dict['Assay_ID'].append(current_assay_index)
                    else:
                        #If now tissue is available retrieve only the cellType Specificity and Distribution.
                        #create an identifier of the current element.
                        uniq_specificity_str = (join_attributes_to_str(subelem)+rna_distribution_str).upper()
                        if uniq_specificity_str not in rna_specificity_helper_dict:
                            #if new identifier, store new one and all the data
                            current_specificity_id = len(rna_specificity_dict['RNA_Specificity_ID'])
                            rna_specificity_helper_dict[uniq_specificity_str] = current_specificity_id
                            rna_specificity_dict['RNA_Specificity_ID'].append(current_specificity_id)
                            #rna_specificity_dict['rnaSpecificity_description'].append(subelem.attrib['description'])
                            #rna_specificity_dict['rnaSpecificity_specificity'].append(subelem.attrib['specificity'])
                            rna_specificity_dict['rnaSpecificity_description'].append('')
                            rna_specificity_dict['rnaSpecificity_specificity'].append(subelem.attrib['category'])
                            rna_specificity_dict['Tissue_ID'].append('')
                            rna_specificity_dict['rnaDistribution_description'].append(rna_distribution_str)
                            gene_assay_rnaSpecificity_link_dict['RNA_Specificity_ID'].append(current_specificity_id)
                        else:
                            #if old identifier, retrieve index.
                            gene_assay_rnaSpecificity_link_dict['RNA_Specificity_ID'].append(
                                rna_specificity_helper_dict[uniq_specificity_str])
                        #Fill in the linker table per Gene/ Assay combination for which
                        #RNA Specificity is defined.
                        gene_assay_rnaSpecificity_link_dict['Gene-RNASpecificity-link_ID'].append(
                            len(gene_assay_rnaSpecificity_link_dict['Gene-RNASpecificity-link_ID']))
                        gene_assay_rnaSpecificity_link_dict['Gene_ID'].append(current_gene_index)
                        gene_assay_rnaSpecificity_link_dict['Assay_ID'].append(current_assay_index)

                #if there is singleCellTypeExpression, then it occurs multiple times therefore I
                #have to iterate over all of them
                for subelem in element.iterfind('singleCellTypeExpression'):
                    current_tissue_index = retrieve_tissue(subelem,tissue_info_dict,
                                                           current_assay_index)[0]
                    value_dict['Tissue_ID'].append(current_tissue_index)
                    value_dict['Gene_ID'].append(current_gene_index)
                    value_dict['Assay_ID'].append(current_assay_index)
                    #fill in RNA Expression values to value_dict.
                    current_value_id = retrieve_level(subelem,value_dict)
                    
                #The cluster results are done outside of the retrieval of data from 
                #singleCellTypeExpression, but immediately subsequent thus current indexes can be 
                #used.
                #because the current ensembl ID is always the last in the list it is used to access
                #data in scRNA_df
                current_batch_df = scRNA_df.loc[gene_info_dict['Ensembl_ID'][-1]]

                #retrieves tissue information from scRNA_file for the current EnsemblID
                current_sc_ids = retrieve_tissue_scRNA(tissue_info_dict,
                                                       current_batch_df['Cluster'].values.astype('|S5'),
                                                       current_batch_df['Tissue'].values.astype('|S20'),
                                                       current_batch_df['Cell type'].values.astype('|S30'),
                                                       current_tissue_index)

                #Writing the clustered scRNAseq results to scRNA_cellType_dict.
                scRNA_cellType_dict['Tissue_ID'].extend(current_sc_ids)
                scRNA_cellType_dict['Gene_ID'].extend([current_gene_index]*len(current_sc_ids))
                scRNA_cellType_dict['Read_count'].extend(current_batch_df['Read count'])
                scRNA_cellType_dict['pTPM'].extend(current_batch_df['pTPM'])
                scRNA_cellType_dict['scRNA_exp_ID'].extend([*range(len(scRNA_cellType_dict['scRNA_exp_ID']),
                                           len(scRNA_cellType_dict['scRNA_exp_ID'])+
                                           len(current_sc_ids))])
                                   
            #Here I am going to access the clustered scRNAseq results.


            # """
            # !!! Not yet implemented, but can be implemented on this level
            
            #iterfind searches within the xml tree for the tag and iterates over all its children.
            #tissueExpression contains antibody staining data which can also indicate the epxression of genes in tissues.
            #for element in entry.iterfind('tissueExpression'):
                
            #iterfind searches within the xml tree for the tag and iterates over all its children.
            #pathologyExpression RNA expression patterns for cancer tissues and prognostics of survival analysis.
            #for element in entry.iterfind('pathologyExpression'):
            
            #iterfind searches within the xml tree for the tag and iterates over all its children.
            #antibody contains the antigen sequence for staining the tissues in case of RNA expression.
            #for element in entry.iterfind('antibody'):
            # """

            #at this point, all the data for a particular entry (gene) is retrived and the
            #constructed xml tree can be cleared.
            entry.clear()


    ###############################################################
    #Table creation
    ###############################################################

    #converting all dicts to DataFrames
    assay_table = pd.DataFrame(assay_info_dict).set_index('Assay_ID')
    rna_specificity_table = pd.DataFrame(rna_specificity_dict).set_index('RNA_Specificity_ID')
    tissue_info_table = pd.DataFrame(tissue_info_dict).set_index('Tissue_ID')
    value_table = pd.DataFrame(value_dict).set_index('Value_ID')
    RNA_sample_table = pd.DataFrame(RNA_sample_dict).set_index('RNASample_ID')
    gene_info_table = pd.DataFrame(gene_info_dict).set_index('Gene_ID')
    gene_assay_rnaSpecificity_link_table = pd.DataFrame(
        gene_assay_rnaSpecificity_link_dict).set_index('Gene-RNASpecificity-link_ID')
    Protein_information_table = pd.DataFrame(Protein_information_dict).set_index('Protein_info_ID')
    protein_linker_table = pd.DataFrame(protein_linker_dict).set_index('Protein_linker_ID')
    Protein_class_table = pd.DataFrame(Protein_class_dict).set_index('Protein_classes_ID')
    scRNA_cellType_table = pd.DataFrame(scRNA_cellType_dict).set_index('scRNA_exp_ID')
    
    #extracting gtex data and updating tissue table
    gtex_info_table, tissue_info_table = create_gtex_df(args.GTEx_file,
                                                        gene_info_table,
                                                        tissue_info_table)

    #creating an database engine
    engine = create_engine('sqlite:///'+args.HPA_database, echo = True)
    #connect to the created engine
    conn = engine.connect()

    #load in meta data objects (tables)
    meta = MetaData()

    #Initialize SQL Tables
    gene_info = Table(
       'Gene_information', meta,
       Column('Gene_ID', Integer, primary_key = True),
       Column('Ensembl_ID', String),
       Column('HPA_version', String),
       Column('URL', String),
       Column('Name', String),
       Column('assembly', String),
       Column('gencodeVersion', String)
    )

    assay_info = Table(
       'Assay_information', meta,
       Column('Assay_ID', Integer, primary_key = True),
       Column('source', String),
       Column('technology', String),
       Column('assayType', String)
    )

    tissue_info = Table(
       'Tissue_information', meta,
       Column('Tissue_ID', Integer, primary_key = True),
       Column('tissue_name', String),
       Column('ontologyTerms', String),
       Column('cellosaurusID', String),
       Column('additional_information', String)
    )

    RNA_specificity = Table(
       'RNA_specificity', meta,
       Column('RNA_Specificity_ID', Integer, primary_key = True),
       Column('Tissue_ID', Integer, ForeignKey('Tissue_information.Tissue_ID')),
       Column('rnaSpecificity_description', String),
       Column('rnaSpecificity_specificity', String),
       Column('rnaDistribution_description', String)
    )

    RNA_Expression = Table(
       'RNA_Expression', meta,
       Column('Value_ID', Integer, primary_key = True),
       Column('Tissue_ID', Integer, ForeignKey('Tissue_information.Tissue_ID')),
       Column('Gene_ID', Integer, ForeignKey('Gene_information.Gene_ID')),
       Column('Assay_ID', Integer, ForeignKey('Assay_information.Assay_ID')),
       Column('normalizedRNAExpression', Float),
       Column('proteinCodingRNAExpression', Float),
       Column('RNAExpression', Float),
       Column('norm-RNA-exp_unit', String),
       Column('prot-CodingRNA-exp_unit', String),
       Column('RNA-exp_unit', String),
       Column('Text', String)
    )

    RNA_sample = Table(
       'RNA_Samples', meta,
       Column('RNASample_ID', Integer, primary_key = True),
       Column('Tissue_ID', Integer, ForeignKey('Tissue_information.Tissue_ID')),
       Column('Gene_ID', Integer, ForeignKey('Gene_information.Gene_ID')),
       Column('Assay_ID', Integer, ForeignKey('Assay_information.Assay_ID')),
       Column('Value_ID', Integer, ForeignKey('RNA_Expression.Value_ID')),
       Column('expRNA', Float),
       Column('RNA-exp_unit', String),
       Column('sampleId', Integer),
       Column('sex', String),
       Column('age', Integer)
    )

    Gene_Assay_RNA_specificity_linker = Table(
       'Gene_Assay_RNA_specificity_linker', meta,
       Column('Gene-RNASpecificity-link_ID', Integer, primary_key = True),
       Column('Gene_ID', Integer, ForeignKey('Gene_information.Gene_ID')),
       Column('Assay_ID', Integer, ForeignKey('Assay_information.Assay_ID')),
       Column('RNA_Specificity_ID', Integer,ForeignKey('RNA_specificity.RNA_Specificity_ID')),
    )

    Protein_information = Table(
       'Protein_information', meta,
       Column('Protein_info_ID', Integer, primary_key = True),
       Column('Gene_ID', Integer, ForeignKey('Gene_information.Gene_ID')),
       Column('database', String),
       Column('protId', String)
    )

    Protein_linker = Table(
       'Protein_linker', meta,
       Column('Protein_linker_ID', Integer, primary_key = True),
       Column('Protein_info_ID', Integer, ForeignKey('Protein_information.Protein_info_ID')),
       Column('Protein_classes_ID', Integer, ForeignKey('Protein_classes.Protein_classes_ID'))
    )

    Protein_classes = Table(
       'Protein_classes', meta,
       Column('Protein_classes_ID', Integer, primary_key = True),
       #Column('Protein_linker_ID', Integer, ForeignKey('Protein_linker.Protein_linker_ID')),
       Column('source', String),
       Column('ProteinClass_description', String)
    )
    
    scRNA_cellType = Table(
       'scRNA_cellType', meta,
       Column('scRNA_exp_ID', Integer, primary_key = True),
       Column('Tissue_ID', Integer, ForeignKey('Tissue_information.Tissue_ID')),
       Column('Gene_ID', Integer, ForeignKey('Gene_information.Gene_ID')),
       Column('Read_count', Integer),
       Column('pTPM', Float)
    )

    gtex_expression = Table(
       'GTEx_expression', meta,
       Column('GTEx_exp_ID', Integer, primary_key = True),
       Column('Gene_ID', Integer, ForeignKey('Gene_information.Gene_ID')),
       Column('Tissue_ID', Integer, ForeignKey('Tissue_information.Tissue_ID')),
       Column('Expression', Float),
    )

    #create all tables.
    meta.create_all(engine)

    #Store the Pandas DataFrames as SQL in the pre-initialized tables.
    gene_info_table.to_sql('Gene_information', con=conn, if_exists='append')
    assay_table.to_sql('Assay_information', con=conn, if_exists='append')
    tissue_info_table.to_sql('Tissue_information', con=conn, if_exists='append')
    gene_assay_rnaSpecificity_link_table.to_sql('Gene_Assay_RNA_specificity_linker',
                                                con=conn,
                                                if_exists='append')
    rna_specificity_table.to_sql('RNA_specificity', con=conn, if_exists='append')
    value_table.to_sql('RNA_Expression', con=conn, if_exists='append')
    RNA_sample_table.to_sql('RNA_Samples', con=conn, if_exists='append')
    Protein_information_table.to_sql('Protein_information', con=conn, if_exists='append')
    protein_linker_table.to_sql('protein_linker', con=conn, if_exists='append')
    Protein_class_table.to_sql('Protein_classes', con=conn, if_exists='append')
    scRNA_cellType_table.to_sql('scRNA_cellType', con=conn, if_exists='append')
    gtex_info_table.to_sql('gtex_expression', con=conn, if_exists='append')
    
    #close connection
    conn.close()

    #take final time and print it as hours/minutes/seconds
    end = time.time()
    print('Programme finished after (h/m/s):\n',(end-start)/3600,(end-start)/60,end-start)
    #15.535551427867677 932.1330856720607 55927.98514032364


    ###############################################################
    ########################   FINISHED   #########################
    ###############################################################

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def set_args(description='Generates the HPA SQLite database from XML'):
    """
    Setup command line arguments but do not parse them

    Parameters
    ----------
    description : str, optional, default: `Generates the HPA SQLite database from XML`
        The program description

    Returns
    -------
    parser : `argparse.ArgumentParser`
        A parser with all the arguments set up within it
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--HPA_database',
                        type=str,
                        default="HPA_v05.db",
                        help='path to sql database that is supposed to be generated, if file \
                            already exists it will stop the execution of the script.')
    parser.add_argument('--infile',
                        type=str,
                        #default="proteinatlas.xml",
                        default="first_ten_entries.xml",
                        help="path to the xml file containing the Human Protein Atlas data.")
    parser.add_argument('--scRNA_file',
                        type=str,
                        default="rna_single_cell_type_tissue.tsv",
                        help="path to the RNA Single Cell Type Tissue file, containing cluster \
                            associations and read counts of single RNA seq studies.")
    parser.add_argument('--GTEx_file',
                        type=str,
                        default="GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_median_tpm.gct",
                        help="path to the GTEx expression file of all Genes that are defined by\
                            Ensembl and and their GTEx tissue specific expression.")        
    return parser

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_args(parser):
    """
    Parse the command line arguments

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        A parser with all the arguments set up within it

    Returns
    -------
    args : :obj:`Namespace`
        An object with all the parsed command line arguments within it
    """
    args = parser.parse_args()
    return args

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def join_children_to_str(el):
    """
    Reads in all children nodes of the current node and joins them to one semicolon
    delimited list, returned as a string

    Parameters
    ----------
    el : xml.etree.ElementTree.Element
        Element whose children elements are supposed to be joined to a ; delimited string.

    Returns
    -------
    str
        String containing a semicolon delimited list of all values of all children nodes.
    """
    subelem_children = list(el)
    temp = []
    for child in subelem_children:
        temp.append(child.tag)
        for key, value in child.attrib.items():
            temp.append(((key+':'+value)))
        temp.append(child.tail.strip())
        temp.append(child.text.strip())

    return ';'.join([x for x in temp if x])

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def join_attributes_to_str(el):
    """
    Reads in all values of the current node and joins them to one semicolon
    delimited list, returned as a string

    Parameters
    ----------
    el : xml.etree.ElementTree.Element
        Element whose values are supposed to be joined to a ; delimited string.

    Returns
    -------
    str
        String containing a semicolon delimited list of all values of the current node.
    """
    temp = []
    for key, value in el.attrib.items():
        temp.append(((key+':'+value)))
    temp.append(el.tail.strip())
    if el.text is not None:
        temp.append(el.text.strip())

    return ';'.join([x for x in temp if x])

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def fill_up_dict(key_comp,temp_dict,keys_fill):
    """
    This function checks the length of the 'key_comp' list within the 'temp_dict'
    and appends empty strings to all lists specified in the list of keys in 'keys_fill'
    until they have the same length.

    Parameters
    ----------
    key_comp : str
        The list in the dictionary which indicates the length for all the other lists.
    temp_dict : dict
        Dictionary containing strings as keys and lists as values. This dictionary must
        contain key_comp and all keys in keys_fill list.
    keys_fill : list
        List of keys of dictionary entries which are supposed to be appended with empty strings
        until they all have the same length.

    Returns
    -------
    temp_dict : dict
        Returns a new dictionary with all lists having the same length.
    """
    temp_int = len(temp_dict[key_comp])
    for i in keys_fill:
        if len(temp_dict[i]) != temp_int:
            temp_dict[i].append('')
    return temp_dict

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def retrieve_single_entry(tag,elem):
    """
    Retrieves the first element of the specified name.

    Parameters
    ----------
    tag : str
        Name of the subelement which is supposed to be returned.
    elem : xml.etree.ElementTree.Element
        XML Element within which the specified subelement is searched.

    Returns
    -------
    element : xml.etree.ElementTree.Element
        The searched subelement
    """
    for element in elem.iterfind(tag):
        return element

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def retrieve_proteininfo(subelement, dictionary,current_gene_index):
    """
    This function searches the handed over xml element for the keyword 'identifier' which children
    contains protein identifier of the current gene.

    Parameters
    ----------
    subelement : xml.etree.ElementTree.Element
        XML Element within which the search is conducted.
    dictionary : dict
        The handed over dictionary must be the Protein_information_dict. As an mutuable object,
        the handed over dictionary will be altered and filled with data if avbailable
        without specifically returning it.

    Returns
    -------
    int
        Returns the number of protein identifier for the current gene.
    """
    if retrieve_single_entry('identifier',subelement) is not None:
        if list(retrieve_single_entry('identifier',subelement)) != []:
            for i,child in enumerate(list(retrieve_single_entry('identifier',subelement))):
                dictionary['Protein_info_ID'].append(len(dictionary['Protein_info_ID']))

                dictionary['Gene_ID'].append(current_gene_index)

                dictionary['protId'].append(child.attrib['id'])
                dictionary['database'].append(child.attrib['db'])

            return i+1
        else:
            return 0
    else:
        return 0

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def retrieve_proteinclass(subelement, dictionary,current_prot_id):
    """
    This function searches the handed over xml element for the keywords 'proteinClasses' and
    'proteinClass'. These elements contain protein class descriptions. The data stored within
    these elements will be stored in the handed over dictionary.

    Parameters
    ----------
    subelement : xml.etree.ElementTree.Element
        XML Element within which the search is conducted.
    dictionary : dict
        The handed over dictionary must be the Protein_class_dict. As an mutuable object,
        the handed over dictionary will be altered and filled with data if avbailable
        without specifically returning it.

    Returns
    -------
    None
        No Value is returned.
    """
    for child in retrieve_single_entry('proteinClasses',subelement).iterfind('proteinClass'):
        #per child one linker row will be initalized
        protein_linker_dict['Protein_info_ID'].append(current_prot_id)
        current_protein_linker_id = len(protein_linker_dict['Protein_linker_ID'])
        protein_linker_dict['Protein_linker_ID'].append(current_protein_linker_id)

        temp_uniq_id = join_attributes_to_str(child).upper()
        if temp_uniq_id not in protein_helper_dict:
            current_protein_class_id = len(dictionary['Protein_classes_ID'])
            protein_helper_dict[temp_uniq_id] = current_protein_class_id

            dictionary['Protein_classes_ID'].append(current_protein_class_id)
            dictionary['source'].append(child.attrib['source'])
            dictionary['ProteinClass_description'].append(child.attrib['name'])
            #dictionary['Protein_linker_ID'].append(current_protein_linker_id)
        else:
            current_protein_class_id = protein_helper_dict[temp_uniq_id]

        protein_linker_dict['Protein_classes_ID'].append(current_protein_class_id)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def retrieve_tissue(subelement, dictionary,current_assay_index):
    """
    This function iterates over the children of the current element and stores any tissue related
    information in the provided dictionary. Tissue data which is considered has the tag, tissue,
    region, cellLine, bloodCell, lineage.

    Parameters
    ----------
    subelement : xml.etree.ElementTree.Element
        XML Element over whosse children the iteration is performed.
    dictionary : dict
        The handed over dictionary must be the tissue_info_dict. As an mutuable object,
        the handed over dictionary will be altered and filled with data if avbailable
        without specifically returning it.

    Returns
    -------
    int
        Returns the number of protein identifier for the current gene.
    """
    current_tissue_indexs = []
    for child in subelement.iter():
        if (child.tag in ['tissue','region','cellLine','bloodCell','lineage','cellType']):
            #I assume that tissue, bloodCell or cellLine always occur before level, therefore
            #the current_tissue should always contain the tissue for which the data is stored.
            #3 different tables.
            
            #here I add current_assay_index to make the uniq identifier more uniq.
            #in case of tissue assay, set the index to the same as consensus tissue because both
            #are equivalent in this case.
            #0, 1 are equivalent to each other
            #2, 5 ,7 are equivalent to each other
            #3 ,4 ,6 are equivalent to each other
            if current_assay_index==1:
                temp_uniq_id = ('0'+join_attributes_to_str(child)).upper()
            elif current_assay_index==5 or current_assay_index==7:
                temp_uniq_id = ('2'+join_attributes_to_str(child)).upper()
            elif current_assay_index==4 or current_assay_index==6:
                temp_uniq_id = ('3'+join_attributes_to_str(child)).upper()
            else:
                temp_uniq_id = (str(current_assay_index)+join_attributes_to_str(child)).upper()
            
            if temp_uniq_id not in tissue_helper_dict:
                #add the uniq identifier
                #dictionary['temp_helper'].append(temp_uniq_id)
                 #generate a new ID number and add it to the list.
                current_tissue = len(dictionary['Tissue_ID'])
                tissue_helper_dict[temp_uniq_id] = current_tissue

                dictionary['Tissue_ID'].append(current_tissue)
                #Fills the dict with values
                if child.tag == 'tissue':
                    if 'organ' in child.attrib:
                        key = 'organ'
                    elif 'region' in child.attrib:
                        key = 'region'
                    else:
                        print('Warning, there are different attributes defined than expected in \
                              child %s'%child)
                    dictionary['tissue_name'].append(key+':'+child.attrib[key])
                elif child.tag == 'region':
                    dictionary['tissue_name'].append('region:'+child.text)
                elif child.tag == 'cellLine':
                    dictionary['tissue_name'].append('cellLine:'+child.attrib['organ'])
                elif child.tag == 'bloodCell':
                    dictionary['tissue_name'].append('bloodCell:'+child.attrib['lineage'])
                elif child.tag == 'lineage':
                    dictionary['tissue_name'].append('lineage:'+child.text)
                elif child.tag == 'cellType':
                    dictionary['tissue_name'].append('cellType:'+child.text)
                else:
                    print("no tissue name could be created %s"%temp_uniq_id)

                try:
                    dictionary['cellosaurusID'].append(child.attrib['cellosaurusID'])
                except KeyError:
                    dictionary['cellosaurusID'].append('')

                try:
                    dictionary['ontologyTerms'].append(child.attrib['ontologyTerms'])
                except KeyError:
                    dictionary['ontologyTerms'].append('')
                dictionary['additional_information'].append(child.text)

            else: #return current tissue id such it can be used in the level level
                current_tissue = tissue_helper_dict[temp_uniq_id]
            current_tissue_indexs.append(current_tissue)
        #singleCellTypeEpxression needs to be treated seperatly because it contains expression
        #data on the same level and always would create a unique identifier.
        elif child.tag == 'singleCellTypeExpression':
            temp_uniq_id = (str(current_assay_index)+child.attrib['name']).upper()
            if temp_uniq_id not in tissue_helper_dict:
                #add the uniq identifier
                #dictionary['temp_helper'].append(temp_uniq_id)
                 #generate a new ID number and add it to the list.
                current_tissue = len(dictionary['Tissue_ID'])
                tissue_helper_dict[temp_uniq_id] = current_tissue
                dictionary['Tissue_ID'].append(current_tissue)
                dictionary['tissue_name'].append('cellType:'+child.attrib['name'])
                dictionary['cellosaurusID'].append('')
                dictionary['ontologyTerms'].append('')
                dictionary['additional_information'].append(child.attrib['name'])
            else: #return current tissue id such it can be used in the level level
                current_tissue = tissue_helper_dict[temp_uniq_id]
            current_tissue_indexs.append(current_tissue)
    try:
        return current_tissue_indexs
    except NameError:
        return ['']

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def retrieve_level(subelement, dictionary):
    """
    This function searches the handed over xml element for the keyword 'level' or 
    'singleCellTypeExpression' and iterates over its children. The children of the 
    'level' node contain the different RNA Expression values which are
    stored in the handed over dictionary and form one row in the RNA Expression table.
    singleCellTypeExpression does not have any children.

    Parameters
    ----------
    subelement : xml.etree.ElementTree.Element
        XML Element within which the search is conducted.
    dictionary : dict
        The handed over dictionary must be the value_dict. As an mutuable object,
        the handed over dictionary will be altered and filled with data if avbailable
        without specifically returning it.

    Returns
    -------
    int
        The function returns the RNA Expression row index.
    """
    if len(list(subelement.iterfind('level')))>0:
        dictionary['Value_ID'].append(len(dictionary['Value_ID']))
        #m!!! maybe for loop needs to be lowered
        for child in subelement.iterfind('level'):
            #because these attributes I am quering here are children of the current node, I can
            #only check after the end of the node if all dictionary fields are filled, which i
            #do with fill_up_dict
            if child.attrib['type'] == 'normalizedRNAExpression':
                dictionary['normalizedRNAExpression'].append(child.attrib['expRNA'])
                dictionary['norm-RNA-exp_unit'].append(child.attrib['unitRNA'])
            elif child.attrib['type'] == 'proteinCodingRNAExpression':
                dictionary['proteinCodingRNAExpression'].append(child.attrib['expRNA'])
                dictionary['prot-CodingRNA-exp_unit'].append(child.attrib['unitRNA'])
            elif child.attrib['type'] == 'RNAExpression':
                dictionary['RNAExpression'].append(child.attrib['expRNA'])
                dictionary['RNA-exp_unit'].append(child.attrib['unitRNA'])
            elif child.attrib['type'] == 'expression':
                if child.text is not None:
                    dictionary['Text'].append(child.text)
            else:
                print('there are more nodes in this level %s which have not been \
                      considered yet.'%child)

    elif subelement.tag == 'singleCellTypeExpression' :
        dictionary['Value_ID'].append(len(dictionary['Value_ID']))
        dictionary['normalizedRNAExpression'].append(subelement.attrib['expRNA'])
        dictionary['norm-RNA-exp_unit'].append(subelement.attrib['unitRNA'])

    dictionary = fill_up_dict('Value_ID',
                              dictionary,
                              ['normalizedRNAExpression',
                               'proteinCodingRNAExpression',
                               'RNAExpression',
                               'norm-RNA-exp_unit',
                               'prot-CodingRNA-exp_unit',
                               'RNA-exp_unit',
                               'Text']
                              )
    #returns the last value_id because that is the current level
    return dictionary['Value_ID'][-1]

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def retrieve_RNAsample(subelement,
                       dictionary,
                       current_tissue_index,
                       current_gene_index,
                       current_assay_index,
                       current_value_id):
    """
    This function searches the handed over xml element for the keyword 'RNASample' and iterates
    over its children. The children of the 'RNASample' node contain the different RNA Expression
    Sample data which are stored in the handed over dictionary.

    Parameters
    ----------
    subelement : xml.etree.ElementTree.Element
        XML Element within which the search is conducted.
    dictionary : dict
        The handed over dictionary must be the value_dict. As an mutuable object,
        the handed over dictionary will be altered and filled with data if avbailable
        without specifically returning it.

    Returns
    -------
    None
        No value returned.
    """
    for child in subelement.iterfind('RNASample'):
        dictionary['RNASample_ID'].append(len(dictionary['RNASample_ID']))
        dictionary['Tissue_ID'].append(current_tissue_index)
        dictionary['Gene_ID'].append(current_gene_index)
        dictionary['Assay_ID'].append(current_assay_index)
        dictionary['Value_ID'].append(current_value_id)
        try:
            dictionary['sampleId'].append(child.attrib['sampleId'])
        except KeyError:
            dictionary['sampleId'].append('')
        try:
            dictionary['expRNA'].append(child.attrib['expRNA'])
        except KeyError:
            dictionary['expRNA'].append('')
        try:
            dictionary['age'].append(child.attrib['age'])
        except KeyError:
            dictionary['age'].append('')
        try:
            dictionary['sex'].append(child.attrib['sex'])
        except KeyError:
            dictionary['sex'].append('')
        try:
            dictionary['RNA-exp_unit'].append(child.attrib['unitRNA'])
        except KeyError:
            dictionary['RNA-exp_unit'].append('')

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def retrieve_tissue_scRNA(dictionary,cluster_values,tissue_values,celltype_values,current_tissue_index):
    """
    Retrieves tissue information from batch of the scRNA_file and stores it in 
    tissue_information dict. 

    Parameters
    ----------
    cluster_values : numpy.array
        Containing the cluster column of the scnRNA_file data batch.
    tissue_values : numpy.array
        Containing the tissue column of the scnRNA_file data batch.
    celltype_values : numpy.array
        Containing the cellType column of the scnRNA_file data batch.
    current_tissue_index : int
        Indicates the index of the current tissue

    Returns
    -------
    None.
    """        
    #creating an array with unique tissue identifiers 
    uniq_cell_ar = np.core.defchararray.add(cluster_values,
                                            np.core.defchararray.add(
                                                celltype_values,
                                                tissue_values)
                                            ).astype(str)
    #creating a boolean array indicating new tissues
    #this can also be used to access values in cluster_array and cluster_tissue_ar
    new_tissues_bool = ~np.isin(uniq_cell_ar,[*tissue_helper_dict.keys()])
    
    #counting how many unique identifiers are not present in the helper_dict
    num_new_tissues = np.sum(new_tissues_bool)
    if num_new_tissues>0:
        new_uniq_cell_ar = uniq_cell_ar[new_tissues_bool]
        
        #creating array of new tissue indexes
        new_tissue_indexes = range(current_tissue_index+1,current_tissue_index+num_new_tissues+1)
        #updating the helper dict with unique identifiers and indexes
        tissue_helper_dict.update(dict(zip(new_uniq_cell_ar,[*new_tissue_indexes])))
        
        #this unpacks the tissue_helper_dict and a newly created dict and joins them in a new one
        #because a new dict is created instead of updating an old one, it is considered a local 
        #variable and will cause an error because I rely on manipulating globablly defined dicts.      
        #tissue_helper_dict = {**tissue_helper_dict, **dict(zip(uniq_cell_ar,
        #                                                       [*new_tissue_indexes]
        #                                                       ))}
    
        #creates a numpy array containing the joint cluster_values and tissue_values of new values.
        cluster_tissue_ar = np.core.defchararray.add(cluster_values[new_tissues_bool],
                                                     np.core.defchararray.add(
                                                         np.array(
                                                             [':']*cluster_values[new_tissues_bool].size,
                                                             dtype='|S1'),
                                                         tissue_values[new_tissues_bool])
                                                     ).astype(str)        
    
        #returns the tissue_ids for all the new uniq_tissues in uniq_cell_ar
        new_uniq_ids = list(map(tissue_helper_dict.get, new_uniq_cell_ar))
    
        #adding ids to tissue_info_dict
        dictionary['Tissue_ID'].extend(new_uniq_ids)
        #adding tissue_names to tissue_info_dict
        dictionary['tissue_name'].extend(cluster_tissue_ar)
        #adding additional information to tissue_info_dict
        dictionary['additional_information'].extend(celltype_values[new_tissues_bool].astype(str))
        #these values are never given for singleCellTypeExpression cluster data therefore the 
        #tissue_information_dict is filled up with empty strings.
        dictionary['ontologyTerms'].extend(['']*len(new_uniq_ids))
        dictionary['cellosaurusID'].extend(['']*len(new_uniq_ids))
    
    uniq_ids = list(map(tissue_helper_dict.get, uniq_cell_ar))
    
    return uniq_ids

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#GTEx retrieval functions
def set_file_handle(infile):
    """
    Sets the file handle depending on the suffix of the input file. If ending on .gz it opens via
        gzip.open otherwise 
    open.

    Parameters
    ----------
    infile : str
        Path to file.

    Returns
    -------
    file_handle _io.TextIOWrapper: 
        File handle of the opened file.

    """
    if infile.endswith('.gz'):
        file_handle = gzip.open(infile)
    else:
        file_handle = open(infile)
    return file_handle

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def gtex_file_extract(file_handle):
    """
    This function checks the number of columns in the specified GTEx file and retrieves its data
    as an 4 column numpy array by stacking the tissues vertically 

    Parameters
    ----------
    file_handle : _io.TextIOWrapper
        File handle of the GTEx file.

    Returns
    -------
    np.array
        numpy array containing the gene specific arrays, stacked vertically.

    """
    #making sure that the file_handle is set at the beginning of the file
    file_handle.seek(0)
    #initialize output list
    output_ls = []
    #estimate the number of columns in the file by looking at the mode of the first 10 rows.
    num_columns_ls = []
    for i,lines in enumerate(file_handle):
        if i > 10:
            #compute the mode to determine the expected number of columns
            mode_num_columns = max(set(num_columns_ls), key=num_columns_ls.count)
            #set file handle back to start
            file_handle.seek(0)
            break
        line = lines.strip().split('\t')
        num_columns_ls.append(len(line))
    
    #iterating through the file and yielding the required information.
    switch = True
    for lines in file_handle:
        line = lines.strip().split('\t')
        if  len(line) != mode_num_columns:
            continue
        
        #the first line with the correct number of columns is the header
        if switch:
            #fetch header line of the GTEx file, contains the Name, Description and tissue names,
            #only the tissue names need to be kept.
            tissue_ls = line[2:]
            switch = False
            continue
        
        expression_val = line[2:]
        #create lists with repeated ids and names as often as there are expression values.        
        gene_id = [line[0].split('.')[0]]*len(expression_val)
        gene_name = [line[1]]*len(expression_val)
        
        output_ls.append(np.array([gene_id, gene_name, tissue_ls, expression_val]).T)
    
    return np.vstack(output_ls)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_gtex_df(gtex_infile,gene_table,tissue_table):
    """
    This function reads in the file path of the GTEx Gene Tissue expression file and extracts
    its data, transforms it and stores it in a specific DataFrame while simultanously updating the
    tissue_info_table.

    Parameters
    ----------
    gtex_infile : str
        path to GTEx file.
    tissue_table : pd.DataFrame
        Containing all the tissue information from the previous HPA extractions. It is updated
        in this function and returned a new.

    Returns
    -------
    gtex_df : pd.DataFrame
        DataFrame containing the GTEx gene tissue expression data.
    tissue_table : pd.DataFrame
        Tissue_information_table updated with GTEx tissues.

    """
    #gtex_infile = '/Users/christiangross/Documents/data/GTEx/GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_median_tpm.gct'
    gtex_file = set_file_handle(gtex_infile)
    
    gtex_df = pd.DataFrame(gtex_file_extract(gtex_file),
                           columns=['Ensembl_ID', 'Name', 'Tissue', 'Expression'])
    
    #linking the gtex file to the gene_info_table via Ensembl_ID, removing all double information.
    gtex_df = gtex_df.merge(gene_table['Ensembl_ID'].reset_index(),how='left',on='Ensembl_ID')
    gtex_df.dropna(inplace=True)
    gtex_df['Gene_ID'] = gtex_df['Gene_ID'].astype(int)
    del gtex_df['Ensembl_ID']
    del gtex_df['Name']
    
    #GTEx tissues are converted to fit the tissue_info_table format and then added 
    gtex_tissues_ls = [x.split(' - ') for x in gtex_df['Tissue'].unique()]
    tmp_list = []
    for tissue in gtex_tissues_ls:
        if len(tissue) == 1:
            tmp_list.append(['GTEx:'+tissue[0],'','',''])
        else:
            tmp_list.append(['GTEx:'+tissue[0],'','',tissue[1]])
    
    gtex_tissue_df = pd.DataFrame(tmp_list,
                                  columns=['tissue_name',
                                           'ontologyTerms',
                                           'cellosaurusID',
                                           'additional_information'],
                                  index=gtex_df['Tissue'].unique())
    
    tissue_table = tissue_table.append(gtex_tissue_df,
                                                 ignore_index=True)
    
    tissue_table.index.name = 'Tissue_ID'
    gtex_tissue_df['Tissue_ID'] = tissue_table[tissue_table.tissue_name.str.contains('GTEx:')].index
    gtex_df['Tissue_ID'] = gtex_tissue_df.loc[gtex_df.Tissue]['Tissue_ID'].values
    del gtex_df['Tissue']
    gtex_df.reset_index(drop=True,inplace=True)
    gtex_df.index.name = 'GTEx_exp_ID'
    
    return gtex_df,tissue_table


"""
#!!!my first attempts to fuzzy check GTEx tissues and already existing tissues in the tissue_info_table, might be implemented in v.06
matching_tissues_ar = np.array([tissue_info_table['tissue_name'].str.contains("Substantia nigra",case=False),tissue_info_table['additional_information'].str.contains("Substantia nigra",case=False)])
matching_tissues_ls = []
matching_information_ls = []
matching_tissues_ls.append(tissue_info_table['tissue_name'].str.contains("Substantia nigra",case=False))
matching_information_ls.append(tissue_info_table['additional_information'].str.contains("Substantia nigra",case=False))
"""

###############################################################
#Executing main
###############################################################
if __name__ == "__main__":
    main()


# '''
# !!!This code helps to see the number of entries in each dictionary.

# [(len(assay_info_dict[x]),x) for x in assay_info_dict.keys()]
# [(len(rna_specificity_dict[x]),x) for x in rna_specificity_dict.keys()]
# [(len(tissue_info_dict[x]),x) for x in tissue_info_dict.keys()]
# [(len(value_dict[x]),x) for x in value_dict.keys()]
# [(len(RNA_sample_dict[x]),x) for x in RNA_sample_dict.keys()]
# [(len(gene_info_dict[x]),x) for x in gene_info_dict.keys()]
# [(len(gene_assay_rnaSpecificity_link_dict[x]),x) for x in gene_assay_rnaSpecificity_link_dict.keys()]
# [(len(Protein_information_dict[x]),x) for x in Protein_information_dict.keys()]
# [(len(protein_linker_dict[x]),x) for x in protein_linker_dict.keys()]
# [(len(Protein_class_dict[x]),x) for x in Protein_class_dict.keys()]
# '''

# """
# !!! Time passed using a single core on a home laptop in python

# 0 Time passed: 6.553596920437283e-06
# 1000 Time passed: 0.01671531187163459
# 2000 Time passed: 0.04578326331244575
# 3000 Time passed: 0,08364037361409929
# 4000 Time passed: 0,12946127355098724 465.84 - 459 = 6 sec
# 5000 Time passed: 0,7009220821989908 2523 - 663 = 1860
# 6000 Time passed: 0.7664176630311542
# 7000 Time passed: 0.839367155234019
# 8000 Time passed: 0.9617593391074075
# 9000 Time passed: 1.0538882469468647
# 10000 Time passed: 2.4190285796589324
# 11000 Time passed: 2.5261019644472333
# 12000 Time passed: 9.623591450254123
# 13000 Time passed: 13.327011395229233
# 14000 Time passed: 13.660012459688716
# 15000 Time passed: 13.779659081631237
# 16000 Time passed: 14.838568182720078
# 17000 Time passed: 14.99091614888774
# 18000 Time passed: 15.129856076902813
# 19000 Time passed: 15.256613644427723

# """

# """
# !!! Time passed using a single core on a home laptop in nuitka generated & compiled C code. 
# 0 Time passed: 9.807745615641275e-06
# 1000 Time passed: 0.01501627829339769
# 2000 Time passed: 0.045328595240910846
# 3000 Time passed: 0.08228856417867872
# 4000 Time passed: 0.12751743025249906
# 5000 Time passed: 0.1844277877940072
# 6000 Time passed: 0.24834881524244945
# 7000 Time passed: 0.3229910055796305
# 8000 Time passed: 0.4079383213652505
# 9000 Time passed: 0.5186101133293576
# 10000 Time passed: 0.6198280486133364
# 11000 Time passed: 1.465874723593394
# 12000 Time passed: 1.5740823321872288
# 13000 Time passed: 4.909492287503348
# 14000 Time passed: 5.024969560768869
# 15000 Time passed: 5.142820091909832
# 16000 Time passed: 8.45540853023529
# 17000 Time passed: 13.156161284976536
# 18000 Time passed: 13.292558204995261
# 19000 Time passed: 13.419050351646211
# Traceback (most recent call last):
#   File "/Users/christiangross/Documents/scripts/hpa-database/./hpa_database_creation.dist/hpa_database_creation.py", line 912, in <module>
#   File "/Users/christiangross/Documents/scripts/hpa-database/./hpa_database_creation.dist/hpa_database_creation.py", line 363, in main
#   File "/Users/christiangross/Documents/scripts/hpa-database/./hpa_database_creation.dist/sqlalchemy/engine/__init__.py", line 520, in create_engine
#   File "/Users/christiangross/Documents/scripts/hpa-database/./hpa_database_creation.dist/sqlalchemy/engine/strategies.py", line 61, in create
#   File "/Users/christiangross/Documents/scripts/hpa-database/./hpa_database_creation.dist/sqlalchemy/engine/url.py", line 172, in _get_entrypoint
#   File "/Users/christiangross/Documents/scripts/hpa-database/./hpa_database_creation.dist/sqlalchemy/util/langhelpers.py", line 277, in load
# sqlalchemy.exc.NoSuchModuleError: Can't load plugin: sqlalchemy.dialects:sqlite
# 1h50mins faster
# """
